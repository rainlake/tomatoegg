#define PEERID_PREFIX             "-TR2810-"
#define USERAGENT_PREFIX          "2.81"
#define SVN_REVISION              "14128"
#define SVN_REVISION_NUM          14128
#define SHORT_VERSION_STRING      "2.81"
#define LONG_VERSION_STRING       "2.81 (14128)"
#define VERSION_STRING_INFOPLIST  2.81
#define MAJOR_VERSION             2
#define MINOR_VERSION             81
#define TR_STABLE_RELEASE         1
